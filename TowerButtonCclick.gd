extends Area2D

onready var global = get_node('/root/global')

func _input_event(viewport, event, shape_idx):
	if event is InputEventMouseButton and event.button_index == BUTTON_LEFT and event.is_pressed() :
		var nodeName = self.get_name().replace("Button", "button").replace("Area", "Image")
		var sprite = get_node(nodeName.replace("Image", "") + "/" + nodeName)
		var child = sprite.duplicate()
		if(global.cursor_sprite != child and global.cursor_sprite):
			global.cursor_sprite.queue_free()
		global.cursor_sprite = child
		