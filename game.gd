extends Node2D

onready var global = get_node('/root/global')
onready var time = 0.0
var max_enemy_num = 128
var enemy_num = 0
var start_countdown = 10.0
var victory = false
# Current level scene
var level_name = ""
var level = null

const ARROW_CURSOR = "res://assets/ui/UIpack_RPG/PNG/cursorHand_grey32.png"
var arrow_cursor = null

func _ready():
	print("Starting new level: " + level_name)
	victory = false
	if level_name == "":
		level_name = "level_1"
	var scn = ResourceLoader.load("res://maps/" + level_name + ".tscn")
	level = scn.instance()
	add_child(level)
	move_child(level, 0)
	set_process(true)
	set_process_input(true)
	arrow_cursor = load(ARROW_CURSOR)
	Input.set_custom_mouse_cursor(arrow_cursor)
func _process(delta):
	get_node("Cash/CashInfo").set_text(str(global.cash))
