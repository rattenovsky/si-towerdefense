extends Node

onready var global = get_node('/root/global')
onready var level = global.level_node

func _ready():
	add_to_group('LevelListeners')
	

func on_level_change():
	level = global.level_node
