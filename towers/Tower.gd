extends Sprite

var time = 0.0
var health = 300
var armor = 10
var damage = 10
var cost = 5
var fire_range = 250
var fire_delta = 1.0/5.0
var fire_next = 0.0
onready var global = get_node('/root/global')
onready var level = global.level_node
const ammunition = "res://towers/weapons/Arrow.tscn"

func _ready():
	var body = self.get_child(0)
	var pos = position + Vector2(0,16)
	var arrajka = []
	for x in range (-16, 17, 16):
		for y in range (-16, 17, 16):
			arrajka.append(position + Vector2(x,y))
	if global.cash >= cost:
		#TODO: Place on good position and delete from AStar (then need to recalculate path in Global) - dla Adama
		var astar_point = level.astar.get_closest_point(Vector3(global_position.x, global_position.y + 16, 0.0))
		var astar_pos = level.astar.get_point_position(astar_point)
		#print([astar_point, astar_pos])
		#print(arrajka)
		if check_astars():
			for node in get_tree().get_nodes_in_group("AstarPoints"):
				if node.position == position:
					print(astar_pos, node.global_position)
					var tastar_point = level.tower_astar.get_closest_point(Vector3(node.global_position.x, node.global_position.y + 16, 0.0))
					var tastar_pos = level.tower_astar.get_point_position(tastar_point)
					node.queue_free()
			
			level.astar.remove_point(astar_point)
			global.decrease_cash(cost)
		else:
			queue_free()
	else:
		queue_free()
			#body.move_and_collide(Vector2(-1 * float(x), -1 * float(y)))
func _physics_process(delta):
	time += delta
	#if enemy_at_range > 0:
	fire()			
	
func choose_target():
	var target = null
	var pos = get_global_position()
	for enemy in get_tree().get_nodes_in_group("enemy"):
		if pos.distance_to(enemy.get_global_position()) <= fire_range:
			if target == null or enemy.get_global_position().x > target.get_global_position().x:
				target = enemy
	return target

func fire():
	if time > fire_next:
		var target_enemy = choose_target()
		if target_enemy == null:
			return
		var scene = load(ammunition)
		var bullet = scene.instance()
		#bullet.set_position(get_position())
		bullet.direction = (target_enemy.get_global_position() - get_global_position()).normalized()
		bullet.level = level
		#rotate_turret(bullet.direction)
		add_child(bullet)
		move_child(bullet, 0)
		fire_next = time + fire_delta
		#get_node("AudioGunshot").play()
			
func check_astars():
	for x in range(-32,32,32):
		for y in range(-32, 32, 32):
			if (x == 0 and y != 0) or (x != 0 and y == 0) or (x != 0 and y != 0):
				var astar_point = level.tower_astar.get_closest_point(Vector3(global_position.x + x, global_position.y + y + 16, 0.0))
				var astar_pos = level.tower_astar.get_point_position(astar_point)
				if position.distance_to(Vector2(astar_pos.x, astar_pos.y)) <= 16:
					print([position, astar_pos, x, y])
					return false
	return true