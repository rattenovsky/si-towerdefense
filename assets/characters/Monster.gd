extends KinematicBody2D

onready var anim = get_node("AnimationPlayer")
onready var initial_pos = position
onready var parent = get_parent()
onready var Brain = get_node('/root/Brain')
onready var global = get_node('/root/global')
onready var level = global.level_node
onready var king_tower = level.get_node('KingTower')
#onready var brain = Brain.new()
var counter = 0
var moves = [ "up", "right", "down",  "left" ]
var path = []
var _dir = null
var speed = 80
var random = 1
var health = 100.0
onready var time = 0.0


# Number of Armor points
var armor = 2

# Number of damage points done by the unit when it reaches its goal
var damage = 10

# Number of coins gained when the unit is destroyed
var reward = 5

# Number of seconds since the unit died
var dead_since = 0

func set_health(val):
	health = val
	get_node("HealthLabel").set_text(str(health))
	
func _ready():
	add_to_group("enemy")
	changeDir("right")
	#anim.get_animation("right").set_loop(true)
	#anim.play("right", -1, speed / 40)
	get_node("HealthLabel").set_text(str(health))
	set_physics_process(true)

func test_collision(dir = 0):
	var pos = position
	var vec = Vector2()
	var collision_len = 8.0
	match moves[dir]:
		"up":
			vec = Vector2(0.0, -collision_len)
		"down":
			vec = Vector2(0.0, collision_len)
		"left":
			vec = Vector2(-collision_len, 0.0)
		"right":
			vec = Vector2(collision_len, 0.0)
	var test = test_move(Transform2D(0.0, pos), vec)
	#print(test, dir)
	if test:
		if dir + 1 < moves.size():
			test_collision(dir + 1)
		else:
			return false
	else:
		return [test, moves[dir]]
func get_vector(dir):
	var vec = Vector2()
	var collision_len = 8.0
	var dir_new = dir + 1
	if dir + 1 >= moves.size():
		dir_new = 0
	match moves[dir_new]:
		"up":
			vec = Vector2(0.0, -collision_len)
		"down":
			vec = Vector2(0.0, collision_len)
		"left":
			vec = Vector2(-collision_len, 0.0)
		"right":
			vec = Vector2(collision_len, 0.0)
	return vec

func _physics_process(delta):
	var pos = global_position
	var vec = Vector2()
	var collision_len = 9.0
	match _dir:
		"up":
			vec = Vector2(0.0, -collision_len)
		"down":
			vec = Vector2(0.0, collision_len)
		"left":
			vec = Vector2(-collision_len, 0.0)
		"right":
			vec = Vector2(collision_len, 0.0)
	var collision = test_move(Transform2D(0.0, pos), vec)
	## TELEPORTY NA KOLIZJI
	#var collision2 = test_move(Transform2D(0.0, pos), vec)
	#var collision_dir = moves.find(_dir)
	#if collision2:
	#	collision2 = test_move(Transform2D(0.0, pos), get_vector(moves.find(collision_dir)))
	#	collision_dir += 1
	#	if collision2:
	#		collision2 = test_move(Transform2D(0.0, pos), get_vector(moves.find(collision_dir)))
	#		collision_dir += 1
	#		if collision2:
	#			collision2 = test_move(Transform2D(0.0, pos), get_vector(moves.find(collision_dir)))
	#			collision_dir += 1
	#		else:
	#			if collision_dir >= moves.size():
	#				collision_dir = 0
	#	else:
	#		if collision_dir >= moves.size():
	#			collision_dir = 0
	#	collision_dir = collision_dir % moves.size()
	#	if moves[collision_dir] != _dir:
	#		var vec_astar = Vector3()
	#		match moves[collision_dir]:
	#			"up":
	#				vec_astar = Vector3(global_position.x,global_position.y - 16, 0.0)
	#			"down":
	#				vec_astar = Vector3(global_position.x,global_position.y + 16, 0.0)
	#			"left":
	#				vec_astar = Vector3(global_position.x - 16,global_position.y, 0.0)
	#			"right":
	#				vec_astar = Vector3(global_position.x + 16,global_position.y, 0.0)
	#		var my_astar = level.astar.get_closest_point(vec_astar)
	#		var target_astar = level.astar.get_closest_point(Vector3(king_tower.position.x - 32, king_tower.position.y + 32, 0.0))
	#		var astar_path = level.astar.get_point_path(my_astar, target_astar)
	#		var curve = parent.get_parent().curve
	#		curve.clear_points()
	#		for point in astar_path:
	#			curve.add_point(Vector2(point.x, point.y))
	#		parent.offset = 0
	#		changeDir(moves[collision_dir])
	## KONIEC TELEPORTOW
	var path_line = Line2D.new()
	path_line.width = 3
	path_line.default_color = Color(22.0 / 255.0, 160.0 / 255.0, 133.0 / 255.0)
	for idx in path.size() - 1:
		var from
		var to
		if idx == 0:
			from = position
		else:
			from = path[idx - 1] - global_position
		to = path[idx]
		path_line.add_point(from)
		path_line.add_point(to - global_position)
	#get_child(get_child_count() - 1).queue_free()
	#add_child(path_line)
		

	if health > 0:
		if parent.get_offset() < 1.0:
			parent.set_offset(parent.get_offset() + speed * delta)
		self.z_index = global_position.y
		time = 0.0
		var new_pos = global_position
		var diff = new_pos - pos
		if(diff.x < 0):
			changeDir('left')
		elif(diff.x >= 0):
			changeDir('right')
		if(diff.y <= -1):
			changeDir('up')
		elif(diff.y >= 1):
			changeDir('down')
	else:
		anim.stop()
		time += delta
		var col = test_collision()
		if col:
			changeDir(col[1])
		#changeDir('up')
	if time >= 5.0:
		hit(health / 2, true)
		time = 0.0
	counter = counter + 1
	if health <= 0:
		anim.stop()
		if dead_since > global.DEAD_CLEAN_INTVAL:
			queue_free()
		else:
			dead_since += delta
		return
	#else:
	#	if counter >= 60:
			#random = randi()%4
	#		counter = 0
	#		changeDir(moves[random])
	#	move(moves[random], delta)
		
	
func changeDir(dir):
	#dir = dir
	#print([dir, _dir])
	if dir != _dir:
		anim.get_animation(dir).set_loop(true)
		anim.play(dir, -1, speed / 40)
		_dir = dir

func hit(damage, continuous=false):
	if health <= 0:
		# If unit is already dead (case of wrecks)
		return
	if not continuous:
		health -= max(0, damage - armor)
	else:
		health -= damage
	get_node("HealthLabel").set_text(str(health))
	#var progress = get_node("HealthProgress")
	#if not progress.is_visible():
	#	progress.show()
	#progress.set_value(health)
	# Death
	if health <= 0:
		print(get_name(), " destroyed!")
		get_node("HealthLabel").hide()
		rotate(PI / 2.0)
		translate(Vector2(0,8))
		#progress.hide()
		# Add explosion
		#var scene = preload("res://explosion-big.tscn")
		#var explosion = scene.instance()
		#explosion.set_position(get_global_position())
		#get_node("/root").add_child(explosion)
		# Add wreckage
		#scene = preload("res://wreck-a.tscn")
		#var wreck = scene.instance()
		#wreck.set_position(get_global_position())
		#wreck.set_frame(randi() % wreck.get_hframes())
		#var root = get_node("/root")
		#global.current_level.level.get_node("DetailsTileMap").add_child(wreck)

		#var texture = ImageTexture.new()
		#texture.load("res://assets/images/tank-a-dead.png")
		#var sprite = get_node("Sprite")
		#sprite.set_texture(texture)
		#sprite.set_hframes(1)
		#sprite.set_frame(0)
		#remove_from_group("enemy")

		# Add label for reward
		#scene = preload("res://ascending-label.tscn")
		#var label = scene.instance()
		#label.set_text("+ $" + str(reward))
		#add_child(label)
		global.increase_cash(reward)
