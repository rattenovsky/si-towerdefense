extends KinematicBody2D

# class member variables go here, for example:
# var a = 2
# var b = "textvar"
onready var anim = get_node("AnimationPlayer")
onready var initial_pos = position
var counter = 0
var moves = [ "up", "right", "down", "left" ]
var motion = Vector2()
const GRAVITY = 20
const SPEED = 5
var random = 1

func _ready():
	anim.get_animation(moves[random]).set_loop(true)
	anim.play(moves[random])
	get_node("HealthLabel").set_text(str(global.health))
	add_to_group("HealthListeners")
#
func on_health_update():
	get_node("HealthLabel").set_text(str(global.health))
func _process(delta):
	counter = counter + 1
	if counter == 60:
		random = randi()%4
		counter = 0
		anim.get_animation(moves[random]).set_loop(true)
		anim.play(moves[random])
	move(moves[random], delta)
		
func move(dir, delta):
	match dir:
		"up":
			move_and_collide(Vector2(0, -1 * SPEED * delta))
		"right":
			move_and_collide(Vector2(1 * SPEED * delta, 0))
		"down":
			move_and_collide(Vector2(0, 1 * SPEED * delta))
		"left":
			move_and_collide(Vector2(-1 * SPEED * delta, 0))
		_:
			pass
