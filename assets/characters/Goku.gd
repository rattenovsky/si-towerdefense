extends 'Monster.gd'

func _ready():
	set_health(800)
	# Number of Armor points
	armor = 2
	
	# Number of damage points done by the unit when it reaches its goal
	damage = 40
	
	# Number of coins gained when the unit is destroyed
	reward = 50

