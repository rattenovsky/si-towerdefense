extends 'Monster.gd'

func _ready():
	set_health(2000)
	# Number of Armor points
	armor = 20
	speed = 20
	
	# Number of damage points done by the unit when it reaches its goal
	damage = 4
	
	# Number of coins gained when the unit is destroyed
	reward = 30

