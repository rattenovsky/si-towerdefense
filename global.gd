extends Node

const DEAD_CLEAN_INTVAL = 3.0

# GAME
var current_level = null
onready var time = 0.0
# PLAYER
var cash = 100
var health = 500
var debug = true
var cursor_sprite = null
var enemy_scenes = {}
var level_node = null


func _ready():
	var root = get_tree().get_root()
	current_level = root.get_child( root.get_child_count() - 1)
	# Preload enemies' scenes
	enemy_scenes["monster-red"] = preload("res://assets/characters/MonsterRed.tscn")
	enemy_scenes["cell"] = preload("res://assets/characters/Cell.tscn")
	enemy_scenes["goku"] = preload("res://assets/characters/Goku.tscn")
	enemy_scenes["tank"] = preload("res://assets/characters/Tank.tscn")
func _input(event):
	if event is InputEventMouseMotion:
		if cursor_sprite:
			var child
			if not get_node(str(level_node.get_path())).has_node(cursor_sprite.get_name()):
				child = get_node(str(level_node.get_path())).add_child(cursor_sprite)
			else:
				child = get_node(str(level_node.get_path()) + '/' + cursor_sprite.get_name())
			if child:
				var tmp_pos = event.position - Vector2(level_node.position.x + 32, level_node.position.y + 32)
				var astar_point = level_node.astar.get_closest_point(Vector3(tmp_pos.x, tmp_pos.y, 0.0))
				var astar_pos = level_node.astar.get_point_position(astar_point)
				child.scale = Vector2(1,1)
				if check_astars(astar_pos):
					child.modulate = Color(0, 1, 0, 0.5)
				else:
					child.modulate = Color(1, 0, 0, 0.5)
				child.position = Vector2(astar_pos.x, astar_pos.y)
				child.z_index = astar_pos.y + 16
			#print(child)
	if event is InputEventMouseButton and event.is_pressed() and event.button_index == BUTTON_LEFT and event.position.y > 160:
		if cursor_sprite:
			var has_collided = false
			var tower_resource
			match cursor_sprite.get_name():
				'buttonTower1Image':
					tower_resource = load('res://towers/ArchersTower.tscn')
				'buttonTower2Image':
					tower_resource = load('res://towers/MainTower.tscn')
			var tower = tower_resource.instance()
			var tmp_pos = event.position - Vector2(level_node.position.x + 32, level_node.position.y + 32)
			var astar_point = level_node.astar.get_closest_point(Vector3(tmp_pos.x, tmp_pos.y, 0.0))
			var astar_pos = level_node.astar.get_point_position(astar_point)
			
			# TODO: Recalculate all Monster's Path
			tower.z_index = astar_pos.y + 16
			tower.global_position = Vector2(astar_pos.x, astar_pos.y)
			get_node(str(level_node.get_path())).add_child(tower)
	if event is InputEventMouseButton and event.is_pressed() and event.button_index == BUTTON_RIGHT and cursor_sprite:
		cursor_sprite = null
			
			
func hit_fortress(damage):
	if level_node:
		if health > 0:
			decrease_health(damage)
			if health <= 0:
				pass
				#level_node.get_node("KingTower/King").queue_free()
func increase_cash(sum):
	cash += sum
	# Update listeners
	get_tree().call_group("CashListeners","on_cash_update")
	
func decrease_cash(sum):
	cash -= sum
	# Update listeners
	get_tree().call_group("CashListeners","on_cash_update")
	
func increase_health(point):
	health += point
	# Update listeners
	get_tree().call_group("HealthListeners","on_health_update")

func decrease_health(point):
	health -= point
	# Update listeners
	get_tree().call_group("HealthListeners","on_health_update")
func change_level(level):
	level_node = level
	print(level.size)
	get_tree().call_group("LevelListeners","on_level_change")
	
func vec3_to_vec2(vec3, axis):
    var array = [vec3.x, vec3.y, vec3.z]
    array.remove(axis)
    return Vector2(array[0], array[1])

func vec2_to_vec3(vec2, axis, value):
    var array = [vec2.x, vec2.y]
    array.insert(axis, value)
    return Vector3(array[0], array[1], array[2])
func check_astars(pos):
	for x in range(-32,32,32):
		for y in range(-32, 32, 32):
			if (x == 0 and y != 0) or (x != 0 and y == 0) or (x != 0 and y != 0):
				var astar_point = level_node.astar.get_closest_point(Vector3(pos.x + x, pos.y + y + 16, 0.0))
				var astar_pos = level_node.astar.get_point_position(astar_point)
				if Vector2(pos.x,pos.y).distance_to(Vector2(astar_pos.x, astar_pos.y)) <= 32:
					#print([position, astar_pos, x, y])
					return false
	return true