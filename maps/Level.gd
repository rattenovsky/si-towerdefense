extends Node2D

onready var global = get_node('/root/global')
onready var time = 0.0
onready var enemy_spawn = 2.0
var astar = AStar.new()
var tower_astar = AStar.new()
onready var ground = get_node('Ground')
onready var game = get_node('/root/Game')
onready var terrain_addons = get_node('Ground/TerrainAddons')
onready var king_tower = get_node('KingTower')
#onready var king_tower_cells = king_tower.get_used_cells()
onready var bridges = get_node('Ground/Bridges')
onready var cells = ground.get_used_cells()
onready var ground_cells = ground.get_used_cells_by_id(0)
onready var bridges_cells = bridges.get_used_cells()
onready var target = get_node('Target')
var astar_point_node = preload("res://AstarPoint.tscn")
var enemy_num = 0
var start_countdown = 3.0
var victory = false
# Enemy waves
var waves = []
# Current enemy wave
var wave_idx = 0
var wave = {}
var size = Vector2()
const WAIT     = 0
const ACTIVE   = 1
const FINISHED = 2
var spawners_count = 6

func init_wave(idx):
	#print(waves)
	wave_idx = idx
	wave = waves[idx]
	wave["count"] = wave["enemies"].size()
	wave["state"] = WAIT
	wave["idx"] = 0
	start_countdown = wave["intval"]
	
func _ready():
	victory = false
	var file = File.new()
	file.open("res://waves/%s.json" % get_name().replace('Level', 'level_'), File.READ)
	var txt = file.get_as_text()
	var rc = parse_json(txt)
	if not rc:
		print("ERROR: Failed to parse wave file")
	waves = rc["waves"]
	init_wave(0)
	set_process(true)
	set_process_input(true)
	#for bridge in bridges.get_used_cells():
	#	var cell = bridges.get_cell(bridge.x, bridge.y)
	#	if [1170, 1171, 1172].has(cell):
			#print(cell)
	#		ground_cells.append(bridge - (ground.position / 16) + Vector2(1,1))
	
	for idx in ground_cells.size() - 1:
		var cell = ground_cells[idx]
		var cell_pos = ground.map_to_world(cell)
		if terrain_addons.get_cellv(cell + (ground.position / 16)) < 0:
			astar.add_point(idx, Vector3(cell_pos.x + ground.position.x, cell_pos.y +  ground.position.y, 0.0))
			tower_astar.add_point(idx, Vector3(cell_pos.x + ground.position.x, cell_pos.y +  ground.position.y, 0.0))
			var point_inst = astar_point_node.instance()
			point_inst.add_to_group('AstarPoints')
			point_inst.position = Vector2(cell_pos.x + ground.position.x, cell_pos.y +  ground.position.y)
			add_child(point_inst)
			var checks = []
			checks.append(astar.get_closest_point(Vector3(cell_pos.x + ground.position.x, cell_pos.y +  ground.position.y - 16, 0.0)))
			checks.append(astar.get_closest_point(Vector3(cell_pos.x + ground.position.x + 16, cell_pos.y +  ground.position.y, 0.0)))
			checks.append(astar.get_closest_point(Vector3(cell_pos.x + ground.position.x, cell_pos.y +  ground.position.y + 16, 0.0)))
			checks.append(astar.get_closest_point(Vector3(cell_pos.x + ground.position.x - 16, cell_pos.y +  ground.position.y, 0.0)))
			for check in checks:
				if check != idx:
					
					var pos_p = astar.get_point_position(check)
					var pos_i = astar.get_point_position(idx)
					var line = Line2D.new()
					if (pos_p.x == pos_i.x or pos_p.y == pos_i.y) and pos_p.distance_to(pos_i) <= 16:
						astar.connect_points(idx, check)
						line.width = 3
						line.default_color = Color(22.0 / 255.0, 160.0 / 255.0, 133.0 / 255.0)
					#if pos_p.distance_to(pos_i) >= 0:
						line.add_point(Vector2(pos_p.x,pos_p.y))
						line.add_point(Vector2(pos_i.x,pos_i.y))
						#add_child(line)
			#for point in astar.get_points():
				#if point != idx:
					#var pos_p = astar.get_point_position(point)
					#var pos_i = astar.get_point_position(idx)
					#if(pos_p.x == pos_i.x or pos_p.y == pos_i.y):
						#astar.connect_points(point, idx)
						#if global.debug:
							#var line = Line2D.new()
							#line.width = 3
							#line.add_point(Vector2(pos_p.x,pos_p.y))
							#line.add_point(Vector2(pos_i.x,pos_i.y))
							#add_child(line)
							#pass
	
func _process(delta):
	time += delta
	if wave["state"] == ACTIVE:
		if wave["idx"] < wave["count"]:
			if time > enemy_spawn:
				# Spawn next enemy in waves
				# (idx is already incremented)
				var enemy = wave["enemies"][wave["idx"]]
				var scene = global.enemy_scenes[enemy["type"]]
				var enemy_scn = scene.instance()
				
				# ASTAR CURVE
				var points = astar.get_points()
				var path = Path2D.new()
				var curve = Curve2D.new()
				#for point in points:
				#	var point_pos = astar.get_point_position(point)
				#	var checks = []
				#	checks.append(astar.get_closest_point(Vector3(point_pos.x, point_pos.y - 16, 0.0)))
				#	checks.append(astar.get_closest_point(Vector3(point_pos.x + 16, point_pos.y, 0.0)))
				#	checks.append(astar.get_closest_point(Vector3(point_pos.x, point_pos.y + 16, 0.0)))
				#	checks.append(astar.get_closest_point(Vector3(point_pos.x - 16, point_pos.y, 0.0)))
				#	for check in checks:
				#		if check != point:
				#			var pos_p = astar.get_point_position(check)
				#			if pos_p.distance_to(point_pos) == 16:
				#				curve.add_point(Vector2(pos_p.x, pos_p.y))
				var spawner_number = (randi() % spawners_count) + 1
				var spawner = get_node('Spawner%s' % spawner_number)
				#curve.add_point(spawner.position)
				#curve.add_point(target.position)
				var spawner_astar = astar.get_closest_point(Vector3(spawner.position.x, spawner.position.y, 0.0))
				var target_astar = astar.get_closest_point(Vector3(king_tower.position.x - 32, king_tower.position.y + 32, 0.0))
				var astar_path = astar.get_point_path(spawner_astar, target_astar)
				#print('ASTAR: %s, %s, %s' % [spawner_astar, target_astar, astar_path])
				for point in astar_path:
					curve.add_point(Vector2(point.x, point.y))
					enemy_scn.path.append(Vector2(point.x, point.y))
				path.curve = curve
				var node = get_node('.')
				node.add_child(path)
				
				#var path = Path2D.new()
				#var curve = Curve2D.new()
				#for node in enemy.path:
				#	curve.add_point(Vector2(node.x, node.y))
				#path.curve = curve
				
				var follow = PathFollow2D.new()
				follow.set_loop(false)
				follow.set_rotate(false)
				#follow.v_offset = -1 * (enemy_scn.get_node('Sprite').texture.get_size().y / 8)
				#follow.h_offset = -1 * (enemy_scn.get_node('Sprite').texture.get_size().x / 8)
				#var node = get_node('.')
				#node.add_child(path)
				var path_node = get_child(get_child_count() - 1)
				path_node.add_child(follow)
				path_node.get_child(0).add_child(enemy_scn)
				
				
				#var path = PathFollow2D.new()
				#path.set_loop(false)
				#get_node(enemy["spawner"]).add_child(enemy_scn)
				#path.add_child(enemy)

				if (wave["idx"]+1) < wave["count"]:
					# Next enemy in wave
					wave["idx"] += 1
					enemy_spawn = time + wave["enemies"][wave["idx"]]["intval"]
				else:
					wave["state"] = FINISHED
					if (wave_idx+1) < waves.size():
						# Next waves
						init_wave(wave_idx+1)
			else:
				pass
		else:
			pass
	elif wave["state"] == WAIT:
		start_countdown -= delta
		if start_countdown > 0:
			#get_node("SkullButton/Label").set_text(str(ceil(start_countdown)) + "s")
			pass
		else:
			wave["state"] = ACTIVE
			#get_node("WaveSprite/WaveLabel").set_text(str(wave_idx+1) + "/" + str(waves.size()))
			#get_node("SkullButton").hide()

	#get_node("CashSprite/CashLabel").set_text(str(global.cash))
	#get_node("HealthSprite/HealthLabel").set_text(str(global.health))

	if global.health <= 0:
		print("You loose!")
		set_process(false)
	elif (not victory and
		#waves[waves.size()-1]["state"] == FINISHED and
		wave["state"] == FINISHED and
		get_tree().get_nodes_in_group("enemy").size() == 0):
		print("Victory!")
		victory = true
		#get_node("AnimPlayer").play("Victory")
		set_process(false)

